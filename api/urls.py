from django.urls import path
from .views import operation

urlpatterns = [
	path('<str:operation>/<str:first_number>,<str:second_number>', operation),
]
